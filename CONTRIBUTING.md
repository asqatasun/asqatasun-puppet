# Contributing

First off, thanks for taking the time to contribute! 👍

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md).
By participating in this project you agree to abide by its terms.

## Versioning

- All notable changes to this project will be documented in [CHANGELOG](CHANGELOG.md) file.
- This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
    ```shell
    MAJOR.MINOR.PATCH
       # MAJOR ---> a breaking change: incompatible changes
       # MINOR ---> add a new feature (ex: new Asqtasun version)
       # PATCH ---> fix a bug
    ```

## Contributing guidelines

If you follow these contributing guidelines your patch
will likely be included into a release a little more quickly.

1. Fork the repo.
2. Create a separate branch for your change.
3. We only take merge requests with passing tests, and documentation. You can also execute them locally.
4. Checkout [Voxpupuli review docs](https://voxpupuli.org/docs/reviewing_pr/). We try to
   use it to review a merge request and the [official styleguide](https://puppet.com/docs/puppet/6.0/style_guide.html).
   They provide some guidance for new code that might help you before you submit a merge request.
5. Add a test for your change. Only refactoring and documentation
   changes require no new tests. If you are adding functionality
   or fixing a bug, please add a test.
6. Squash your commits down into logical components.
   Make sure to rebase against our current master.
7. Push the branch to your fork and submit a merge request.

Please be prepared to repeat some of these steps as your code is reviewed.

## Git Commit message convention

We follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) :

```xml
<type>[(optional scope)]: <description>

[optional body]

[optional footer(s)]
```

> common types:
>
> - `build`: changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm).
> - `ci`: changes to the CI configuration files and scripts
> - `chore`: update something without impacting the user (ex: bump a dependency in composer.json).
> - `docs`: documentation only changes
> - `feat`: add a new feature (equivalent to a `MINOR` in Semantic Versioning)
> - `fix`: fix a bug (equivalent to a `PATCH` in Semantic Versioning).
> - `perf`: a code change that improves performance.
> - `refactor`: a code change that neither fixes a bug nor adds a feature.
> - `revert`: revert a commit.
> - `style`: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc).
> - `test`: adding missing tests, refactoring tests; no production code change.

The commit message should follow this regex:

```perl
/^(revert: )?(build|ci|chore|docs|feat|fix|perf|refactor|style|refactor|revert|style|test|)(\(.+\))?: .{1,50}/
```


## Sources of inspiration for CONTRIBUTING.md

- [Voxpupuli/Puppet-Archive contributing file](https://github.com/voxpupuli/puppet-archive/blob/master/.github/CONTRIBUTING.md)
- [Adullact/Puppet-Coturn contributing file](https://gitlab.adullact.net/adullact/puppet-coturn/-/blob/master/CONTRIBUTING.md)
- [Atom contributing file](https://github.com/atom/atom/blob/master/CONTRIBUTING.md) that is really good and brightly written.
- For contributors doc [Gitlab Workflow](https://about.gitlab.com/handbook/#gitlab-workflow)

