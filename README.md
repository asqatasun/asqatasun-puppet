# Puppet Asqatasun

Puppet module for Asqatasun

Work in progress.

## Table of Contents

1. [Description](#description) --> work in progress
1. [Setup - The basics of getting started with asqatasun](#setup)  --> work in progress
    * [What asqatasun affects](#what-asqatasun-affects)  --> work in progress
    * [Setup requirements](#setup-requirements)  --> work in progress
    * [Beginning with asqatasun](#beginning-with-asqatasun)  --> work in progress
1. [Usage - Configuration options and additional functionality](#usage)  --> work in progress
1. [Limitations - OS compatibility, etc.](#limitations)  --> work in progress
1. [Development - Guide for contributing to the module](#contribute)
1. [Contact and discussions](#contact-and-discussions)


## Contribute

We would be glad to have you on board! You can help in many ways:

1. Use Asqatasun on your sites !
1. Give us [**feedback** on the forum](http://forum.asqatasun.org)
1. Contribute to Asqatasun
   - [Fill in bug report to Asqatasun](https://gitlab.com/asqatasun/Asqatasun/-/issues)
   - [**Contribute** to Asqatasun](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/CONTRIBUTING.md) code
1. Contribute to Asqatasun **Puppet** module (current repository)
   - [Fill in bug report](https://gitlab.com/asqatasun/asqatasun-puppet/-/issues)
   - [Guide for contributing to the module](https://gitlab.com/asqatasun/asqatasun-puppet/-/blob/master/CONTRIBUTING.md)

## Contact and discussions

- [Asqatasun **Forum**](http://forum.asqatasun.org/)
- [**Twitter** @Asqatasun](https://twitter.com/Asqatasun)
- [Instant messaging on **Element.io**: +asqatasun:matrix.org](https://app.element.io/#/group/+asqatasun:matrix.org)
- email to `asqatasun AT asqatasun dot org` (only English, French and Klingon is spoken :) )
